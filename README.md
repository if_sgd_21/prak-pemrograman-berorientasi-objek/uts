# UTS



## Encapsulation

```php
class Database
{
    //gunakan encapsulation
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $db_name = "twitterclone";
    public $conn;

    function setHost($host)
    {
        $this->host = $host;
    }
    function getHost()
    {
        return $this->host;
    }
    function getUsername()
    {
        return $this->username;
    }
    function getPassword()
    {
        return $this->password;
    }
    function getDb_name()
    {
        return $this->db_name;
    }
    function getConn()
    {
        return $this->conn;
    }
    function __construct()
    {
        $this->conn = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
    }
}
```

## Abstraction
```php
abstract class UserManager extends Database
{
    public string $username;
    public string $password;
    public string $email;
    abstract public function register($data);
    abstract public function cekCookie();
    abstract public function login();
    abstract public function logout();
}
```
## Inheritance & Polymorphism
```php
class Twitter extends UserManager implements TweetManager
{
    //override
    //fungsi register
    function register($data)
    {
        if (isset($_POST["register"])) {
            $username = strtolower(stripslashes($data["username"]));
            $firstname = ucfirst(strtolower(stripslashes($data["firstname"])));
            $lastname = ucfirst(strtolower(stripslashes($data["lastname"])));
            $nickname = strtolower(stripslashes($data["nickname"]));
            $email = strtolower(stripslashes($data["email"]));
            $password = mysqli_real_escape_string($this->getConn(), $data["password"]);
            $password2 = mysqli_real_escape_string($this->getConn(), $data["password2"]);
            $joinDate = date("Y-m-d H:i:s");
            // cek username sudah ada atau belum
            $result = mysqli_query($this->getConn(), "SELECT username FROM users WHERE username = '$username'");
            if (mysqli_fetch_assoc($result)) {
                echo "<script>
                alert('username sudah terdaftar!');
                </script>";
                return false;
            }
            // cek konfirmasi password
            if ($password !== $password2) {
                echo "<script>
                alert('konfirmasi password tidak sesuai!');
                </script>";
                return false;
            }
            // enkripsi password
            $password = password_hash($password, PASSWORD_DEFAULT);

            // tambahkan user baru ke database
            mysqli_query($this->getConn(), "INSERT INTO users (id, username, nickname, first_name, last_name, email, password, join_date) 
            VALUES('', '$username', '$nickname', '$firstname', '$lastname', '$email', '$password', '$joinDate')");

            if (mysqli_affected_rows($this->getConn()) == 1) {
                echo "<script>
        alert('user baru berhasil ditambahkan!');
    </script>";
            } else {
                echo "<script>
        alert('gagal menambahkan user baru: " . mysqli_error($this->getConn()) . "');
    </script>";
                return false;
            }
            return mysqli_affected_rows($this->getConn());

        }
    }
}
```

## Link Youtube

...

